# frozen_string_literal: true

module License
  module Management
    VERSION = '3.31.4'
  end
end
