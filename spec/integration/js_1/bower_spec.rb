# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "bower" do
  include_examples "each report version", "js", "bower", "d36902e5"

  context "when scanning a simple bower project" do
    subject { runner.scan }

    before do
      runner.add_file('bower.json', fixture_file_content('js/bower/bower.json'))
    end

    specify { expect(subject).to match_schema }
    specify { expect(subject.dependency_names).to match_array(['debug', 'cli', 'ev-emitter', 'example-project', 'fizzy-ui-utils', 'get-size', 'jquery', 'masonry-layout', 'matches-selector', 'outlayer', 'stimulus.umd']) }
    specify { expect(subject.licenses_for('cli')).to match_array(['Apache-2.0', 'BSD-2-Clause', 'BSD-3-Clause', 'ISC', 'MIT']) }
    specify { expect(subject.licenses_for('ev-emitter')).to match_array(['MIT']) }
    specify { expect(subject.licenses_for('example-project')).to match_array(['ISC']) }
    specify { expect(subject.licenses_for('fizzy-ui-utils')).to match_array(['MIT']) }
    specify { expect(subject.licenses_for('get-size')).to match_array(['MIT']) }
    specify { expect(subject.licenses_for('jquery')).to match_array(['MIT']) }
    specify { expect(subject.licenses_for('masonry-layout')).to match_array(['MIT']) }
    specify { expect(subject.licenses_for('matches-selector')).to match_array(['MIT']) }
    specify { expect(subject.licenses_for('outlayer')).to match_array(['MIT']) }
    specify { expect(subject.licenses_for('stimulus.umd')).to match_array(['unknown']) }
  end

  context "when scanning a bower project with a dependency from a custom npm registry" do
    subject { runner.scan(env: { 'ADDITIONAL_CA_CERT_BUNDLE' => x509_certificate.read }) }

    before do
      runner.add_file(".npmrc", "registry = https://npm.test")
      runner.add_file(".bowerrc") do
        JSON.pretty_generate({ resolvers: ['bower-npm-resolver'] })
      end
      runner.add_file("bower.json") do
        JSON.pretty_generate({
          name: "js-bower",
          license: "ISC",
          dependencies: { lodash: "npm:lodash#4.17.10" }
        })
      end
    end

    specify do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array(%w[debug js-bower lodash])
      expect(subject.licenses_for('debug')).to match_array(['unknown'])
      expect(subject.licenses_for('js-bower')).to match_array(['ISC'])
      expect(subject.licenses_for('lodash')).to match_array(['MIT'])
    end
  end

  [
    'https://gitlab.com/gitlab-org/ci-training-slides',
    'https://gitlab.com/gitlab-org/frontend/At.js.git',
    'https://gitlab.com/gitlab-org/gitter/gitter-marked.git'
  ].each do |git_repo|
    context "when scanning #{git_repo}" do
      subject { runner.scan }

      before do
        runner.clone(git_repo)
      end

      specify { expect(subject).to match_schema }
      specify { expect(subject.dependency_names).not_to be_empty }
    end
  end
end
