# frozen_string_literal: true

name "asdf"
default_version "v0.7.8"

source git: "https://github.com/asdf-vm/asdf.git"
relative_path "asdf-#{version}"

env = with_embedded_path.merge('ASDF_DATA_DIR' => install_dir)
env['PATH'] = "#{install_dir}/shims:#{env['PATH']}"

build do
  sync project_dir, install_dir
  command "echo 'always_keep_download = yes' >> #{install_dir}/defaults", env: env
  command "echo 'legacy_version_file = yes' >> #{install_dir}/defaults", env: env
  command "echo 'use_release_candidates = no' >> #{install_dir}/defaults", env: env
end

build do
  command "asdf plugin list all", env: env
  # TODO: revert to official asdf plugin once https://github.com/emersonsoares/asdf-dotnet-core/pull/27
  # (Plugin/curl is not downloading the dotnet install script due to a 302 redirect not being handled)
  # has been merged. See https://gitlab.com/gitlab-org/gitlab/-/issues/322120
  command "asdf plugin add dotnet-core https://gitlab.com/willmeek/asdf-dotnet-core.git", env: env
  command "asdf plugin-add elixir", env: env
  # TODO: temporary fix to support version of asdf < 0.8.0 due to lack of ASDF_DOWNLOAD_PATH usage
  # https://github.com/asdf-vm/asdf/blob/master/docs/plugins-create.md#bininstall
  command "asdf plugin add golang https://github.com/gonzoyumo/asdf-golang.git", env: env
  command "asdf plugin-add gradle", env: env
  command "asdf plugin-add java", env: env
  command "asdf plugin-add maven", env: env
  command "asdf plugin-add nodejs", env: env
  command "asdf plugin-add php", env: env
  command "asdf plugin-add python", env: env
  command "asdf plugin-add ruby", env: env
  command "asdf plugin-add rust", env: env
  command "asdf plugin-add sbt", env: env
end

build do
  command "asdf install elixir 1.10.4", env: env
  command "asdf install sbt 1.3.8", env: env
  # trigger download of pyenv
  command "asdf list-all python", env: env
  # trigger download of java cached file list
  command "asdf list-all java", env: env.merge('CACHE_DIR' => install_dir)
  command "asdf reshim", env: env
end

build do
  delete "#{install_dir}/installs/elixir/**/man"
  command "tar -czvf #{install_dir}/plugins.tar.gz #{install_dir}/plugins"
  delete "#{install_dir}/plugins"
end
