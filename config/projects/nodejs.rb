# frozen_string_literal: true

nodejs_version = ENV.fetch('NODE_VERSION', '14.17.1')

name "nodejs-#{nodejs_version}"
maintainer "GitLab B.V."
homepage "https://nodejs.org/"

install_dir "/opt/asdf/installs/nodejs/#{nodejs_version}"
package_scripts_path Pathname.pwd.join("config/scripts/nodejs")

build_version nodejs_version
build_iteration 1

override 'asdf_nodejs', version: nodejs_version
dependency "asdf_nodejs"

package :deb do
  compression_level 9
  compression_type :xz
end
